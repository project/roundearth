<?php

namespace Drupal\roundearth_donate\Controller;

use CiviCRM_API3_Exception;
use Drupal\civicrm\Civicrm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Controller for some donation-related pages.
 */
class DonateController extends ControllerBase {

  /**
   * CiviCRM service.
   *
   * @var \Drupal\civicrm\Civicrm
   */
  protected $civicrm;

  /**
   * Round Earth Donate configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * DonateController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory service.
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $httpKernel
   *   The HTTP kernel.
   * @param \Drupal\civicrm\Civicrm $civicrm
   *   CiviCRM service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, HttpKernelInterface $httpKernel, Civicrm $civicrm) {
    $this->config = $configFactory->get('roundearth_donate.settings');
    $this->httpKernel = $httpKernel;
    $this->civicrm = $civicrm;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_kernel'),
      $container->get('civicrm')
    );
  }

  /**
   * Controller for donate page.
   */
  public function donate() {
    if ($response = $this->servePage($this->config->get('donate_page'))) {
      return $response;
    }

    return [
      '#markup' => $this->t('You need to configure a @type page', [
        '@type' => $this->t('Donate'),
      ]),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
  }

  /**
   * Controller for join page.
   */
  public function join() {
    if ($id = $this->config->get('join_page')) {
      return $this->servePage($id);
    }

    return [
      '#markup' => $this->t('You need to configure a @type page', [
        '@type' => $this->t('Join'),
      ]),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
  }

  /**
   * Use HTTP kernel to serve the appropriate contribution page.
   *
   * @param int $id
   *   Contribution page ID.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  protected function servePage($id) {
    if (!$page = $this->getContribPage($id)) {
      return NULL;
    }

    $params = $_REQUEST;
    $_REQUEST = array_merge($_REQUEST, ['reset' => 1, 'id' => $id]);
    $request = Request::create('/civicrm/contribute/transact', 'GET', $params);
    $response = $this->httpKernel->handle($request, HttpKernelInterface::SUB_REQUEST);
    $_REQUEST = $params;
    return $response;
  }

  /**
   * Gets info for a contribution page.
   *
   * @return array|null
   *   Contribution page data, or NULL if not found/active.
   */
  protected function getContribPage($id) {
    $this->civicrm->initialize();

    try {
      $result = \civicrm_api3('ContributionPage', 'getsingle', [
        'id' => $id,
        'is_active' => 1,
      ]);
    }
    catch (CiviCRM_API3_Exception $e) {
      return NULL;
    }

    return $result;
  }

}
