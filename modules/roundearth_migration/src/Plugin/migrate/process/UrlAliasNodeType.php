<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class UrlAliasNodeType.
 *
 * Analyzes the type of the node which the alias points to, and skips items not
 * in the specified list.
 *
 * Configuration:
 *
 * - 'type': The list of types to look for.
 * - 'negate': Reverses the logic, skipping the listed types.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_url_alias_node_type"
 * )
 */
class UrlAliasNodeType extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TaxonomyTermByName.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Determine source node type.
    if (!$node = $this->getSourceNode($value)) {
      throw new MigrateSkipRowException();
    }

    $matched = in_array($node->bundle(), $this->configuration['types']);
    if ($matched != empty($this->configuration['negate'])) {
      throw new MigrateSkipRowException();
    }

    return $node->bundle();
  }

  /**
   * Gets the node from the alias's source.
   *
   * @param string $source
   *   The alias source.
   *
   * @return \Drupal\node\Entity\Node|null
   *   The migrated node, or NULL if not found.
   *
   * @throws \Drupal\migrate\MigrateSkipRowException
   *   If the path is not a node page or the node does not exist.
   */
  protected function getSourceNode($source) {
    $parts = explode('/', ltrim($source, '/'));
    if (count($parts) != 2 || $parts[0] != 'node') {
      return NULL;
    }

    return $this->getNodeStorage()->load($parts[1]);
  }

  /**
   * Gets the node storage handler.
   *
   * @return \Drupal\node\NodeStorage
   *   The node storage handler.
   */
  protected function getNodeStorage() {
    return $this->entityTypeManager->getStorage('node');
  }

}
