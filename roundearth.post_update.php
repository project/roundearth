<?php

/**
 * Migrates existing file entities to media items, and their field values.
 */
function roundearth_post_update_media_migration() {
  $media_mappings = [];

  /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
  $entityFieldManager = \Drupal::service('entity_field.manager');
  $node_storage = \Drupal::entityTypeManager()->getStorage('node');
  $media_storage = \Drupal::entityTypeManager()->getStorage('media');
  $field_collection_storage = \Drupal::entityTypeManager()->getStorage('field_collection_item');

  /** @var \Drupal\node\NodeInterface[] $content_pages */
  $content_pages = $node_storage->loadByProperties(['type' => 'roundearth_content_page']);
  $fields = $entityFieldManager->getFieldDefinitions('node', 'roundearth_content_page');
  $has_image = !empty($fields['field_roundearth_banner_image']);
  $has_upload = !empty($fields['field_roundearth_uploads']);
  foreach ($content_pages as $content_page) {
    if ($has_image && !$content_page->get('field_roundearth_banner_image')->isEmpty()) {
      // This is an image field with single value.
      /** @var \Drupal\file\FileInterface $image */
      $image = $content_page->get('field_roundearth_banner_image')->first()->entity;
      if (!isset($media_mappings[$image->id()])) {
        /** @var \Drupal\media\MediaInterface $media */
        $media = $media_storage->create([
          'bundle' => 'image',
        ]);
        $media->get('field_media_image')->setValue($image);
        $media->save();
        $media_mappings[$image->id()] = $media;
      }
      else {
        $media = $media_mappings[$image->id()];
      }

      $content_page->get('field_roundearth_banner_image')->setValue(NULL);
      $content_page->get('field_roundearth_media_banner')->setValue($media);
    }
    if ($has_upload && !$content_page->get('field_roundearth_uploads')->isEmpty()) {
      /** @var \Drupal\file\FileInterface[] $files */
      $files = $content_page->get('field_roundearth_uploads')->referencedEntities();
      foreach ($files as $file) {
        if (!isset($media_mappings[$file->id()])) {
          /** @var \Drupal\media\MediaInterface $media */
          $media = $media_storage->create([
            'bundle' => 'file',
          ]);
          $media->get('field_media_file')->setValue($file);
          $media->save();
          $media_mappings[$file->id()] = $media;
        }
        else {
          $media = $media_mappings[$file->id()];
        }
        $content_page->get('field_roundearth_media_uploads')->appendItem($media);
      }
      $content_page->get('field_roundearth_uploads')->setValue(NULL);
    }
    $content_page->save();
  }

  /** @var \Drupal\node\NodeInterface[] $content_news */
  $content_news = $node_storage->loadByProperties(['type' => 'roundearth_news']);
  $fields = $entityFieldManager->getFieldDefinitions('node', 'roundearth_news');
  $has_image = !empty($fields['field_roundearth_featured_image']);
  $has_upload = !empty($fields['field_roundearth_uploads']);
  foreach ($content_news as $content_new) {
    if ($has_image && !$content_new->get('field_roundearth_featured_image')->isEmpty()) {
      // This is an image field with single value.
      /** @var \Drupal\file\FileInterface $image */
      $image = $content_new->get('field_roundearth_featured_image')->first()->entity;
      if (!isset($media_mappings[$image->id()])) {
        /** @var \Drupal\media\MediaInterface $media */
        $media = $media_storage->create([
          'bundle' => 'image',
        ]);
        $media->get('field_media_image')->setValue($image);
        $media->save();
        $media_mappings[$image->id()] = $media;
      }
      else {
        $media = $media_mappings[$image->id()];
      }

      $content_new->get('field_roundearth_featured_image')->setValue(NULL);
      $content_new->get('field_roundearth_media_featured')->setValue($media);
    }
    if ($has_upload && !$content_new->get('field_roundearth_uploads')->isEmpty()) {
      /** @var \Drupal\file\FileInterface[] $files */
      $files = $content_new->get('field_roundearth_uploads')->referencedEntities();
      foreach ($files as $file) {
        if (!isset($media_mappings[$file->id()])) {
          /** @var \Drupal\media\MediaInterface $media */
          $media = $media_storage->create([
            'bundle' => 'file',
          ]);
          $media->get('field_media_file')->setValue($file);
          $media->save();
          $media_mappings[$file->id()] = $media;
        }
        else {
          $media = $media_mappings[$file->id()];
        }
        $content_new->get('field_roundearth_media_uploads')->appendItem($media);
      }
      $content_new->get('field_roundearth_uploads')->setValue(NULL);
    }
    $content_new->save();
  }

   /** @var \Drupal\field_collection\FieldCollectionInterface[] $carousel_slides */
  $carousel_slides = $field_collection_storage->loadByProperties([
    'host_type' => 'block_content',
    'field_name' => 'field_roundearth_carousel_slides',
  ]);
  $fields = $entityFieldManager->getFieldDefinitions('field_collection_item', 'field_roundearth_carousel_slides');
  $has_image = !empty($fields['field_roundearth_carousel_image']);
  foreach ($carousel_slides as $carousel_slide) {
    if ($has_image && !$carousel_slide->get('field_roundearth_carousel_image')->isEmpty()) {
      // This is an image field with single value.
      /** @var \Drupal\file\FileInterface $image */
      $image = $carousel_slide->get('field_roundearth_carousel_image')->first()->entity;
      if (!isset($media_mappings[$image->id()])) {
        /** @var \Drupal\media\MediaInterface $media */
        $media = $media_storage->create([
          'bundle' => 'image',
        ]);
        $media->get('field_media_image')->setValue($image);
        $media->save();
        $media_mappings[$image->id()] = $media;
      }
      else {
        $media = $media_mappings[$image->id()];
      }

      $carousel_slide->get('field_roundearth_carousel_image')->setValue(NULL);
      $carousel_slide->get('field_roundearth_carousel_media')->setValue($media);
    }
    $carousel_slide->save();
  }
}

/**
 * Remove legacy file fields.
 */
function roundearth_post_update_remove_legacy_file_fields() {
  $legacy_entity_fields = [
    'node' => [
      'roundearth_content_page' => [
        'field_roundearth_banner_image',
        'field_roundearth_uploads',
      ],
      'roundearth_news' => [
        'field_roundearth_featured_image',
        'field_roundearth_uploads',
      ],
    ],
    'field_collection_item' => [
      'field_roundearth_carousel_slides' => [
        'field_roundearth_carousel_image',
      ],
    ],
  ];

  /* @var $entityFieldManager Drupal\Core\Entity\EntityFieldManager */
  $entityFieldManager = \Drupal::service('entity_field.manager');

  foreach ($legacy_entity_fields as $entity_type => $legacy_bundle_fields) {
    foreach ($legacy_bundle_fields as $bundle => $legacy_field_names) {
      foreach ($legacy_field_names as $legacy_field_name) {
        $bundle_fields = $entityFieldManager->getFieldDefinitions($entity_type, $bundle);
        if (isset($bundle_fields[$legacy_field_name])) {
          $bundle_fields[$legacy_field_name]->delete();
        }
      }
    }
  }
}

/**
 * Set text formats on message templates.
 */
function roundearth_post_update_update_message_templates() {
  try {
    $storage = \Drupal::entityTypeManager()->getStorage('message_template');
  }
  catch (\Exception $e) {
    return;
  }

  foreach ($storage->loadMultiple() as $template) {
    $template->save();
  }
}
