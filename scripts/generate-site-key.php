#!/usr/bin/env php
<?php

# Write content to sites/default/civicrm.local.inc

if (empty($argv) || empty($argv[1])) {
  print "Must pass a site URL!\n";
  exit(1);
}

$site_key = md5(uniqid('', TRUE) . $argv[1]);

print "<?php\n";
print "\n";
print "define('CIVICRM_SITE_KEY', '$site_key');\n";
print "\n";

