/**
 * @file
 * Jump links implementation.
 */
(function ($, Drupal) {

  Drupal.behaviors.jumpLinks = {
    attach: function (context) {
      var $jumpLinks = $('.jump-links', context);
      var $container = $jumpLinks.find('.container');
      var $select = $jumpLinks.find('select');

      if ($jumpLinks.length) {
        $select.on('change', function () {
          var value = $(this).val();

          $root.animate({
            scrollTop: $('#' + value).offset().top - headerNavHeight
          }, 500);

          return false;
        });

        $select.append('<option value=""> - Jump to section -</option>');

        var $slices = $('.slice', context);
        if ($slices.length) {
          var $root = $('html, body', context);
          var headerNavHeight = $('.navbar-static-top', context).outerHeight() + 50;

          $slices.each(function (index, element) {
            var $slice = $(element);
            var data = $slice.data();
            var id = $slice.attr('id');

            var $link = $('<a href="#' + id + '"><i class="fa fa-' + data.icon + '"></i><span>' + data.title + '</span></a>').on('click', function () {
              $root.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top - headerNavHeight
              }, 500);

              return false;
            });

            if (data.title && id) {
              $container.append($link);
              $select.append('<option value="' + id + '">' + data.title + '</option>')
            }
          });
        }
      }
    }
  };

})(jQuery, Drupal);
